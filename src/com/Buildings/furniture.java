package com.Buildings;

enum furniture  {

    Chair("Chair"),
    Desks("Desks");

    private final String displayName;

    furniture(final String display)
    {
        this.displayName = display;
    }

    @Override public String toString()
    {
        return this.displayName;
    }
}