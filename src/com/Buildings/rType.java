package com.Buildings;

public enum rType {


    Office("Office"),
    Kitchen("Kitchen"),
    Toilet("Toilet"),
    Storage("Storage");
    private final String displayName;

    rType(final String display)
    {
        this.displayName = display;
    }

    @Override public String toString()
    {
        return this.displayName;
    }
}
