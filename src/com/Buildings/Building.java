package com.Buildings;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Building {

    private String buildingName;
    private int floor;
    private List<Floor> floors;

    //constructor
    public Building(String buildingName){
        this.buildingName= buildingName;
        this.floors = new ArrayList<Floor>();
    }

    //getters
    public int getFloorsNumber(){
        return this.floors.size();
    }
    public List<Floor> getFloorList(){return this.floors; }
    public int getFloor(int florNo){
        return this.floors.indexOf(florNo);
    }
    public String getName(){
        return this.buildingName;
    }



    //setters
    public void setBuildingName (String newName){
        this.buildingName=newName;
    }
    public void setFloors(int newFloorNo){
        this.floor=newFloorNo;
    }

    public void addFloor(Floor newFloor){
         this.floors.add(newFloor);
    }
}
