package com.Buildings;

enum Appliance {


    Coffe_Machine("Coffee Machine"),
    Water_Dispenser("Water Dispenser"),
    Fridge("Fridge"),
    TV("TV"),
    Video_Projector("Video Projector"),
    Teleconference("Teleconference");
    private final String displayName;

    Appliance(final String display)
    {
        this.displayName = display;
    }

    @Override public String toString()
    {
        return this.displayName;
    }
}

